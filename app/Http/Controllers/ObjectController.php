<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Objects;

class ObjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllRecords()
    {
        $result = Objects::fetchAllObjects();
        if($result){
            return response()->json([
                "status"  => "success",
                "message" => "Succesful fetch all records",
                "data"    => $result
            ], 200); 
        }
    }

    /**
     * Store a newly created resource in storage
      x
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $objects = $request->all();
        $data = array();
        $timestamp = strtotime("now");
        foreach ($objects as $key => $value) {
            $result = Objects::saveKeyPair($key,$value,$timestamp);
            if($result){
                array_push($data,array(
                    'key'       => $key,
                    'value'     => $value,
                    'timestamp' => $timestamp
                ));
            }
        }   
        if($data){
            return response()->json([
                "status"  => "success",
                "message" => "Key pair values are saved / updated",
                "data"    => $data
            ], 200); 
        } else {
            return response()->json([
                "status"  => "fail",
                "message" => "No key values are found",
                "data"    => $data
            ], 200);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param string $param
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$param)
    {
        if($request->timestamp){
            $result = Objects::fetchObjectByTimestamp($request->timestamp,$param);
            if($result){
                return response()->json([
                    "status"  => "success",
                    "message" => "Success fetching data",
                    "data"    => $result
                ], 200); 
                } else {
                return response()->json([
                    "status"  => "success",
                    "message" => "No key pair value found.",
                    "data"    => null
                ], 200); 
                }
        } else {
            $result = Objects::fetchObjectByKey($param);
            if($result){
            return response()->json([
                "status"  => "success",
                "message" => "Success fetching data",
                "data"    => $result
            ], 200); 
            } else {
            return response()->json([
                "status"  => "success",
                "message" => "No key pair value found.",
                "data"    => null
            ], 200); 
            }
        }
        
    }
}
