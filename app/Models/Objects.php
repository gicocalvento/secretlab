<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Objects extends Model
{
    use HasFactory;
    protected $table = 'objects';
    public $timestamps = true;
    protected $fillable = ['key','value','time_updated','version'];


    /**
     * Fetch all objects in the database returning the latest version of the keys
     * 
     * @return object collection 
     */
    public static function fetchAllObjects()
    {
        return DB::table('objects')->select(DB::raw('`key`,MAX(value) as value,MAX(time_updated) as timestamp'))->groupByRaw('`key`')->orderByRaw('id')->get();
    }

    /**
     * Search an object by identified key
     * 
     * @param string $key 
     * @return object 
     */
    public static function fetchObjectByKey($key)
    {
        return DB::table('objects')->select('key', 'value','time_updated')->where('key', $key)->orderBy('version', 'desc')->first();
    }


    /**
     * Get the attribute name to slugify.
     * 
     * @param integer $timestamp
     * @param string $key 
     * @return object
     */
    public static function fetchObjectByTimestamp($timestamp,$key)
    {
        return DB::table('objects')->select('key', 'value','time_updated')
                        ->where('key', $key)
                        ->where('time_updated','<=',$timestamp)->orderBy('version','desc')->first();
    }

    /**
     * Save or Update a Keypair depending if the key already exists
     * 
     * @param string $key
     * @param string $value
     * @param integer $timestamp
     * @return object
     */
    public static function saveKeyPair($key,$value,$timestamp)
    {
        $result = DB::table('objects')->where('key', $key)->orderBy('version', 'desc')->first();
        if($result){
            $response = Self::create([
                'key'   => $key,
                'value' => $value,
                'version' => $result->version + 1,
                'time_updated' => $timestamp 
            ]);
        } else {
            $response = Self::create([
                'key'   => $key,
                'value' => $value,
                'version' => 1,
                'time_updated' => $timestamp 
            ]);
        }
        return $response;
    }

    
}
