<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ObjectController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/object/get_all_records', [ObjectController::class, 'getAllRecords'])->name('index');
Route::get('/object/{param}', [ObjectController::class, 'show'])->name('show');
Route::post('/object', [ObjectController::class, 'store'])->name('store');
