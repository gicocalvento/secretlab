# Object Key Storage
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
This Application will let you store an object key and keep its versions by passing a timestamp
## Features
- Store an object and get its values via api
- Get all object keys and values
- See different versions of the keys

## Tech

Object Key Generator uses a number of open source projects to work properly:

- Laravel - PHP Framework
- Gitlab - Git Repository
- Gitlab CI - CI/CD Automation
- PHPUnit - Testing Tool/Framework
- Amazon Web Services - EC2 (Web Server) & RDS (MYSQL) (Relational Database)
- Docker - to run our CI/CD Pipelines

## Installation

Object Key Storage requires PHP,Composer,NPM to run.
Install the dependencies and devDependencies and start the server.

```sh
cd secretlab
composer install
npm install
php artisan serve
```

## Testing

Object Key Storage is using PHPunit as testing framework
Install the dependencies and devDependencies and start the server.
```sh
cd secretlab
php artisan test
```

MIT @ Gico Calvento
**Free Software!**