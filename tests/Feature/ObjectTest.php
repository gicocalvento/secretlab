<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ObjectTest extends TestCase
{
    
    public function test_fetch_all_objects()
    {
        
        $response = $this->get('/api/object/get_all_records');
        $response->assertStatus(200);
    }

    public function test_create_new_object()
    {
        $key = 'dummy';
        $value = 'data';
        $response = $this->postJson('/api/object', [$key => $value]);

        $response_data = [
            [
                'key' => $key,
                'value' => $value
            ]
        ];

        $response
            ->assertStatus(200)
            ->assertJsonPath('status', "success")
            ->assertJsonPath('message', "Key pair values are saved / updated")
            ->assertJson([
                'data' => $response_data
            ]);
    }

    public function test_update_existing_object()
    {
        $key = 'dummy';
        $value = 'testing';
        $response = $this->postJson('/api/object', [$key => $value]);

        $response_data = [
            [
                'key' => $key,
                'value' => $value
            ]
        ];

        $response
            ->assertStatus(200)
            ->assertJsonPath('status', "success")
            ->assertJsonPath('message', "Key pair values are saved / updated")
            ->assertJson([
                'data' => $response_data
            ]);
    }

    public function test_get_not_existing_object()
    {
        $key = 'dummy';
        $value = 'testing';
        $response = $this->get('/api/object/notexistingobject');
        $response
            ->assertStatus(200)
            ->assertJsonPath('status', "success")
            ->assertJsonPath('message', "No key pair value found.")
            ->assertJsonPath('data.key', null);
    }

    public function test_get_existing_object()
    {
        $key = 'dummy';
        $response = $this->get('/api/object/dummy');
        $response
            ->assertStatus(200)
            ->assertJsonPath('status', "success")
            ->assertJsonPath('message', "Success fetching data")
            ->assertJsonPath('data.key', $key);
    }

    public function test_get_existing_key_with_timestamp_before_object_creation()
    {
        $key = 'dummy';
        $response = $this->get('/api/object/dummy?timestamp=1624211000');
        $response
            ->assertStatus(200)
            ->assertJsonPath('status', "success")
            ->assertJsonPath('message', "No key pair value found.");
    }

    public function test_get_existing_key_with_timestamp_after_last_object_update()
    {
        $key = 'dummy';
        $response = $this->get('/api/object/dummy?timestamp=1624247500');
        $response
            ->assertStatus(200)
            ->assertJsonPath('status', "success")
            ->assertJsonPath('message', "Success fetching data")
            ->assertJsonPath('data.key', $key);
    }

    public function test_get_existing_key_with_latest_timestamp()
    {
        $key = 'dummy';
        $timestamp = 1624246911;
        $response = $this->get('/api/object/dummy?timestamp=' . $timestamp);
        $response
            ->assertStatus(200)
            ->assertJsonPath('status', "success")
            ->assertJsonPath('message', "Success fetching data")
            ->assertJsonPath('data.key', $key)
            ->assertJsonPath('data.time_updated', $timestamp);
    }

    public function test_get_existing_key_with_first_creation_timestamp()
    {
        $key = 'dummy';
        $timestamp = 1624211324;
        $response = $this->get('/api/object/dummy?timestamp=' . $timestamp);
        $response
            ->assertStatus(200)
            ->assertJsonPath('status', "success")
            ->assertJsonPath('message', "Success fetching data")
            ->assertJsonPath('data.key', $key)
            ->assertJsonPath('data.time_updated', $timestamp);
    }
}

